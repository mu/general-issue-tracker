Use the issue traker: https://gitlab.anu.edu.au/mu/general-issue-tracker/issues

This repository does not have code. Please use the issue tracker for general discussion.

Things not related to any particular components (such as the reference implementation) should be discussed here.

